class Image < ApplicationRecord
  belongs_to :room
  # validation
  validates_presence_of :img_url
end
