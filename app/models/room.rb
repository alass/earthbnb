class Room < ApplicationRecord
  # validations
  validates :title, :location, :price, presence: true
  # associations
  has_many :trips, dependent: :destroy
  has_many :images, dependent: :destroy

  belongs_to :user, optional: true, foreign_key: "host_id"

  include AlgoliaSearch
  algoliasearch per_environment: true do
  end
end
