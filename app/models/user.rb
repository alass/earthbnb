class User < ApplicationRecord
  # Encrypt password
  has_secure_password
  # Model associations
  has_many :trips, foreign_key: :guest_id
  has_many :rooms, foreign_key: :host_id
  has_many :rooms_images, through: :rooms, source: :images
  # Validations
  validates :name, :email, :password_digest, presence: true

  def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
               BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  include AlgoliaSearch
  algoliasearch per_environment: true do
    attribute :name, :email, :created_at, :updated_at
  end
end
