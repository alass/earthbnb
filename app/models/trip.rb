class Trip < ApplicationRecord
  belongs_to :room, optional: true
  belongs_to :user, optional: true, foreign_key: "guest_id"

  validates :room_id, :check_in, :check_out, presence: true

  include AlgoliaSearch
  algoliasearch per_environment: true do
  end
end
