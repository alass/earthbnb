module API::V1
  class RoomsController < ApplicationController
    before_action :set_room, only: %i[show update destroy]
    skip_before_action :authorize_request, only: %i[index show]
    # GET /rooms
    def index
      # Get all rooms
      @rooms = Room.all.paginate(page: params[:page], per_page: 50)
      json_response(@rooms)
    end

    # POST /rooms
    def create
      @room = current_user.rooms.create!(rooms_params)
      json_response(@room, :created)
    end

    # GET /rooms/:id
    def show
      json_response(@room)
    end

    # PUT /rooms/:id
    def update
      @room = current_user.rooms.find(params[:id])
      @room.update(rooms_params)
      head :no_content
    end

    # DELETE /rooms/:id
    def destroy
      @room = current_user.rooms.find(params[:id])
      @room.destroy
      head :no_content
    end

    private

    def rooms_params
      # whitelist params
      params.permit(:title, :location, :price, :id)
    end

    def set_room
      @room = Room.find(params[:id])
    end

  end
end