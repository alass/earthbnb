module API::V1
  class TripsController < ApplicationController
    before_action :set_trip, only: %i[show update destroy]
    # GET /trips
    def index
      @trips = current_user.trips.paginate(page: params[:page], per_page: 20)
      json_response(@trips)
    end

    # POST /trips
    def create
      @trip = Trip.create!(trips_params)
      json_response(@trip, :created)
    end

    # GET /trips/:id
    def show
      @trip = current_user.trips.find(params[:id])
      json_response(@trip)
    end

    # PUT /trips/:id
    def update
      @trip = current_user.trips.find(params[:id])
      @trip.update(trips_params)
      head :no_content
    end

    # DELETE /trips/:id
    def destroy
      @trip = current_user.trips.find(params[:id])
      @trip.destroy
      head :no_content
    end

    private

    def trips_params
      # whitelist params
      params.permit(:room_id, :check_in, :check_out, :guest_id)
    end

    def set_trip
      @trip = Trip.find(params[:id])
    end
  end
end
