module API::V1
  class UsersController < ApplicationController
    before_action :set_user, only: %i[show update destroy]
    skip_before_action :authorize_request, only: %i[create index show]

    # GET /users
    def index
      # Get all users
      @users = User.all.paginate(page: params[:page], per_page: 20)
      json_response(@users)
    end

    # POST /signup
    # return authenticated token upon signup
    def create
      user = User.create!(user_params)
      auth_token = AuthenticateUser.new(user.email, user.password).call
      response = {message: Message.account_created, auth_token: auth_token}
      json_response(response, :created)
    end

    # GET /users/:id
    def show
      json_response(@user)
    end

    # GET /users/me
    def get_me
      @user = current_user
      json_response(@user)
    end

    # PUT /users/me
    def update_me
      @user = current_user
      @user.update(user_params)
      head :no_content
    end

    private

    def user_params
      params.permit(
          :name,
          :email,
          :password,
          :password_confirmation
      )
    end

    def set_user
      @user = User.find(params[:id])
    end
  end
end
