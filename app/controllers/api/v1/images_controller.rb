module API::V1
  class ImagesController < ApplicationController
    before_action :set_room, only: %i[create]
    before_action :set_image, only: %i[show update destroy]
    # GET /images
    def index
      # Get all images
      @images = Image.all
      json_response(@images)
    end

    # POST /images
    def create
      @image = @room.images.create!(images_params)
      json_response(@image, :created)
    end

    # GET /images/:id
    def show
      json_response(@image)
    end

    # PUT /images/:id
    def update
      @image = current_user.rooms_images.find(params[:id])
      @image.update(images_params)
      head :no_content
    end

    # DELETE /images/:id
    def destroy
      @image = current_user.rooms_images.find(params[:id])
      @image.destroy
      head :no_content
    end

    private

    def images_params
      # whitelist params
      params.permit(:img_url, :room_id)
    end

    def set_image
      @image = Image.find(params[:id])
    end

    def set_room
      @room = current_user.rooms.find(params[:room_id])
    end
  end
end