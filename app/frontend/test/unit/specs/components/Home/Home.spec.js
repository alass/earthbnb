import Vue from 'vue';
import {shallow} from 'vue-test-utils';
import test from 'ava';
import Element from 'element-ui';
import Home from '../../../../../src/components/Home/Home.vue';

Vue.use(Element);

test('It should be named Home.', t => {
	const wrapper = shallow(Home);
	t.true(wrapper.name() === 'Home');
});

test('It should render a `<div>`.', t => {
	const wrapper = shallow(Home);
	t.true(wrapper.is('div'));
});
