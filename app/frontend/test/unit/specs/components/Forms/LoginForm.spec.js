import {shallow} from 'vue-test-utils';
import test from 'ava';

import LoginForm from '../../../../../src/components/Forms/LoginForm.vue';

test('It should render a `<div>`.', t => {
	const wrapper = shallow(LoginForm);
	t.true(wrapper.is('el-dialog'));
});

test('It should be named LoginForm.', t => {
	const wrapper = shallow(LoginForm);
	t.true(wrapper.name() === 'LoginForm');
});

test('It should render a dynamic form with email field.', t => {
	const wrapper = shallow(LoginForm, {
		data() {
			return {
				dynamicValidateForm: {
					email: 'email'
				}
			};
		}
	});
	t.true(wrapper.vm.dynamicValidateForm.email !== null);
});

test('It should render a dynamic form with password field.', t => {
	const wrapper = shallow(LoginForm, {
		data() {
			return {
				dynamicValidateForm: {
					password: 'password'
				}
			};
		}
	});
	t.true(wrapper.vm.dynamicValidateForm.password !== null);
});

