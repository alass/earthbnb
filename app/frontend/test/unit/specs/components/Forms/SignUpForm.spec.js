import {shallow} from 'vue-test-utils';
import test from 'ava';

import SignUpForm from '../../../../../src/components/Forms/SignUpForm.vue';

test('It should render a `<div>`.', t => {
	const wrapper = shallow(SignUpForm);
	t.true(wrapper.is('el-dialog'));
});

test('It should be named SignUpForm.', t => {
	const wrapper = shallow(SignUpForm);
	t.true(wrapper.name() === 'SignUpForm');
});

test('It should render a dynamic form with email field.', t => {
	const wrapper = shallow(SignUpForm, {
		data() {
			return {
				dynamicValidateForm: {
					email: 'email'
				}
			};
		}
	});
	t.true(wrapper.vm.dynamicValidateForm.email !== null);
});

test('It should render a dynamic form with name field.', t => {
	const wrapper = shallow(SignUpForm, {
		data() {
			return {
				dynamicValidateForm: {
					name: 'name'
				}
			};
		}
	});
	t.true(wrapper.vm.dynamicValidateForm.name !== null);
});

test('It should render a dynamic form with password field.', t => {
	const wrapper = shallow(SignUpForm, {
		data() {
			return {
				dynamicValidateForm: {
					password: 'password'
				}
			};
		}
	});
	t.true(wrapper.vm.dynamicValidateForm.password !== null);
});

test('It should render a dynamic form with password confirmation field.', t => {
	const wrapper = shallow(SignUpForm, {
		data() {
			return {
				dynamicValidateForm: {
					password_confirmation: 'password'
				}
			};
		}
	});
	t.true(wrapper.vm.dynamicValidateForm.password_confirmation !== null);
});

