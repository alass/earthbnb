import Vue from 'vue';
import Element from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';
import '../styles/main.scss';
import 'element-ui/lib/theme-chalk/index.css';
import App from '../App.vue';
import store from '../src/store/store';
import router from '../src/router';
import {sync} from 'vuex-router-sync';
import _ from 'lodash';
import 'babel-polyfill';
import axios from 'restyped-axios';
import {FETCH_ROOMS} from '../src/store/actions/rooms.action';
import eventBus from '../src/utils/event-bus';
import {USER_REQUEST} from '../src/store/actions/user.action';
import InstantSearch from 'vue-instantsearch';

const token = localStorage.getItem('user-token');

if (token) {
	axios.defaults.headers.common.Authorization = 'Bearer ' + token;
}

Vue.use(Element, {locale});
Vue.use(_);
Vue.use(InstantSearch);

sync(store, router);

document.addEventListener('DOMContentLoaded', () => {
	document.body.appendChild(document.createElement('app'));
	const app = new Vue({
		el: 'app',
		template: '<App/>',
		router,
		store,
		methods: {
			async fetchRooms() {
				await this.$store.dispatch(FETCH_ROOMS)
					.then({
					});
			},
			getUserProfile() {
				if (!token) {
					return;
				}
				this.$store.dispatch(USER_REQUEST)
					.then({});
			},
			handleScroll: () => {
				const AABB = {
					collide: (el1, el2) => {
						const rect1 = el1.getBoundingClientRect();
						const rect2 = el2.getBoundingClientRect();

						return !(
							rect1.top > rect2.bottom ||
							rect1.right < rect2.left ||
							rect1.bottom < rect2.top ||
							rect1.left > rect2.right
						);
					},

					inside: (el1, el2) => {
						const rect1 = el1.getBoundingClientRect();
						const rect2 = el2.getBoundingClientRect();

						return (
							rect1.top <= rect2.bottom
							&& rect1.bottom >= rect2.top
							&& rect1.left <= rect2.right
							&& rect1.right >= rect2.left
						);
					}
				};
				const home = document.querySelector('.fullscreen__bg');
				const header = document.querySelector('.el-menu--horizontal');
				if (home !== null) {
					AABB.collide(home, header) || AABB.inside(home, header)
						? eventBus.$emit('transparentHeader', true)
						: eventBus.$emit('transparentHeader', false);
				} else {
					eventBus.$emit('transparentHeader', false);
				}
			}
		},
		created() {
			this.fetchRooms();
			this.getUserProfile();
			window.addEventListener('scroll', this.handleScroll);
		},
		watch: {
			$route() {
				this.handleScroll();
			}
		},
		render: h => h(App)
	});

	console.log(app);
});
