import Vue from 'vue';
import VueRouter from 'vue-router';
import Room from '../components/Room/Room.vue';
import Home from '../components/Home/Home.vue';
import Trips from '../components/Trips/Trips.vue';
import Listing from '../components/Listing/Listing.vue';
import Discover from '../components/Discover/Discover.vue';
import Host from '../components/Host/Host.vue';

const routes = [
	{name: 'room', path: '/room/:id', component: Room},
	{name: 'home', path: '/', component: Home},
	{name: 'trips', path: '/trips', component: Trips},
	{name: 'listing', path: '/listing', component: Listing},
	{name: 'discover', path: '/discover/:location?', component: Discover},
	{name: 'host', path: '/host', component: Host}
];

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	routes
});

export default router;
