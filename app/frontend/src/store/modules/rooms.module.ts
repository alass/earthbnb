import {
	CREATE_ROOM, CREATE_ROOM_ERROR, CREATE_ROOM_SUCCESS,
	DELETE_ROOM, DELETE_ROOM_ERROR, DELETE_ROOM_SUCCESS, FETCH_ROOMS, SET_ROOMS,
	UPDATE_ROOM, UPDATE_ROOM_ERROR, UPDATE_ROOM_SUCCESS
} from '../actions/rooms.action';
import axios from 'restyped-axios';

import {EarthbnbAPI} from '../../utils/api';

const api = axios.create<EarthbnbAPI>({
	baseURL: window.location.origin + '/api/',
});

const state = {
	status: '',
	rooms: [],
};
const actions = {
	[FETCH_ROOMS]: ({commit}) => {
		return new Promise((resolve, reject) => {
			commit(FETCH_ROOMS);
			api.get('/rooms')
				.then(res => {
					commit(SET_ROOMS, res);
					resolve(res);
				})
				.catch(err => {
					reject(err);
				});
		});
	},
	[DELETE_ROOM]: ({commit, dispatch}, data) => {
		return new Promise((resolve, reject) => {
			commit(DELETE_ROOM);
			api.delete('/rooms/' + data.id)
				.then(res => {
					commit(DELETE_ROOM_SUCCESS, res);
					dispatch(FETCH_ROOMS);
					resolve(res);
				})
				.catch(err => {
					commit(DELETE_ROOM_ERROR, err);
					reject(err);
				});
		});
	},
	[UPDATE_ROOM]: ({commit, dispatch}, data) => {
		return new Promise((resolve, reject) => {
			commit(UPDATE_ROOM);
			api.put('/rooms/' + data.id, data)
				.then(res => {
					commit(UPDATE_ROOM_SUCCESS);
					dispatch(FETCH_ROOMS);
					resolve(res);
				})
				.catch(err => {
					commit(UPDATE_ROOM_ERROR, err);
					reject(err);
				});
		});
	},
	[CREATE_ROOM]: ({commit, dispatch}, data) => {
		return new Promise((resolve, reject) => {
			commit(CREATE_ROOM);
			api.post('/rooms', data)
				.then(res => {
					commit(CREATE_ROOM_SUCCESS);
					dispatch(FETCH_ROOMS);
					resolve(res);
				})
				.catch(err => {
					commit(CREATE_ROOM_ERROR);
					reject(err);
				});
		});
	}
};

const mutations = {
	[SET_ROOMS]: (state, res) => {
		state.rooms = res.data;
	},
	[FETCH_ROOMS]: state => {
		state.status = 'fetching_rooms';
	},
	[DELETE_ROOM]: state => {
		state.status = 'deleting_room';
	},
	[DELETE_ROOM_SUCCESS]: state => {
		state.status = 'room_deleted';
	},
	[DELETE_ROOM_ERROR]: state => {
		state.status = 'room_delete_error';
	},
	[UPDATE_ROOM]: state => {
		state.status = 'updating_room';
	},
	[UPDATE_ROOM_SUCCESS]: state => {
		state.status = 'room_updated';
	},
	[UPDATE_ROOM_ERROR]: state => {
		state.status = 'room_update_error';
	},
	[CREATE_ROOM]: state => {
		state.status = 'loading';
	},
	[CREATE_ROOM_SUCCESS]: state => {
		state.status = 'room_created';
	},
	[CREATE_ROOM_ERROR]: state => {
		state.status = 'room_create_error';
	}
};

const getters = {
	getRooms: state => state.rooms,
	getRoomById: state => id => state.rooms.find(rooms => rooms.id === id),
	getUserRooms: state => hostId => state.rooms.filter(rooms => rooms.host_id === hostId),
	getRoomsByLocation: state => location => state.rooms.filter(rooms => rooms.location === location),
};

export default {
	state,
	mutations,
	actions,
	getters
};
