import {
	USER_REQUEST, USER_ERROR, USER_SUCCESS, UPDATE_PROFILE, UPDATE_PROFILE_SUCCESS,
	UPDATE_PROFILE_ERROR
} from '../actions/user.action';
import {TRIPS_REQUEST} from '../actions/trip.action';
import axios from 'restyped-axios';
import Vue from 'vue';
import {AUTH_LOGOUT} from '../actions/auth.action';
import {User} from '../types/user';
import {EarthbnbAPI} from '../../utils/api';

const api = axios.create<EarthbnbAPI>({
	baseURL: window.location.origin + '/api/'
});

const state: User = {status: '', profile: {}};

const getters = {
	getProfile: state => state.profile,
	isProfileLoaded: state => !!state.profile.name,
};

const actions = {
	[USER_REQUEST]: ({commit, dispatch}) => {
		commit(USER_REQUEST);
		api.get('/users/me')
			.then(res => {
				commit(USER_SUCCESS, res);
				dispatch(TRIPS_REQUEST);
			})
			.catch(err => {
				commit(USER_ERROR);
				dispatch(AUTH_LOGOUT);
			});
	},
	[UPDATE_PROFILE]: ({commit, dispatch}, data) => {
		return new Promise((resolve, reject) => {
			commit(UPDATE_PROFILE);
			api.put('/users/me', data)
				.then(res => {
					commit(UPDATE_PROFILE_SUCCESS);
					dispatch(USER_REQUEST);
					resolve(res);
				})
				.catch(err => {
					commit(UPDATE_PROFILE_ERROR);
					reject(err);
				});
		});
	}
};

const mutations = {
	[USER_REQUEST]: state => {
		state.status = 'loading';
	},
	[USER_SUCCESS]: (state, res) => {
		state.status = 'success';
		Vue.set(state, 'profile', res.data);
	},
	[USER_ERROR]: state => {
		state.status = 'error';
	},
	[UPDATE_PROFILE]: state => {
		state.status = 'update_profile_loading';
	},
	[UPDATE_PROFILE_SUCCESS]: state => {
		state.status = 'update_profile_success';
	},
	[UPDATE_PROFILE_ERROR]: state => {
		state.status = 'update_profile_error';
	},
	[AUTH_LOGOUT]: state => {
		state.profile = {};
	}
};

export default {
	state,
	getters,
	actions,
	mutations,
};
