import {
	TRIPS_REQUEST, TRIPS_SUCCESS, TRIPS_ERROR, TRIPS_DELETE, TRIPS_DELETE_SUCCESS,
	TRIPS_DELETE_ERROR
} from '../actions/trip.action';
import axios from 'restyped-axios';
import {EarthbnbAPI} from '../../utils/api';

const api = axios.create<EarthbnbAPI>({
	baseURL: window.location.origin + '/api/'
});

const state = {status: '', trips: {}};

const getters = {
	getTrips: state => state.trips
};

const actions = {
	[TRIPS_REQUEST]: ({commit}) => {
		return new Promise((resolve, reject) => {
			commit(TRIPS_REQUEST);
			api.get('/trips')
				.then(res => {
					commit(TRIPS_SUCCESS, res);
					resolve(res);
				})
				.catch(err => {
					commit(TRIPS_ERROR);
					reject(err);
				});
		});
	},
	[TRIPS_DELETE]: ({commit, dispatch}, data) => {
		return new Promise((resolve, reject) => {
			commit(TRIPS_DELETE);
			api.delete('/trips/' + data.id, data)
				.then(res => {
					commit(TRIPS_DELETE_SUCCESS, res);
					dispatch(TRIPS_REQUEST);
					resolve(res);
				})
				.catch(err => {
					commit(TRIPS_DELETE_ERROR);
					reject(err);
				});
		});
	}
};

const mutations = {
	[TRIPS_REQUEST]: state => {
		state.status = 'loading';
	},
	[TRIPS_SUCCESS]: (state, res) => {
		state.status = 'success';
		state.trips = res.data;
	},
	[TRIPS_ERROR]: state => {
		state.status = 'error';
	},
	[TRIPS_DELETE]: state => {
		state.status = 'deleting';
	},
	[TRIPS_DELETE_ERROR]: state => {
		state.status = 'delete_error';
	},
	[TRIPS_DELETE_SUCCESS]: state => {
		state.status = 'delete_success';
	}
};

export default {
	state,
	getters,
	actions,
	mutations
};
