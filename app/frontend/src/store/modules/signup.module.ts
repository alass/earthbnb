import {SIGN_UP_REQUEST, SIGN_UP_SUCCESS, SIGN_UP_ERROR} from '../actions/signup.action';
import {USER_REQUEST} from '../actions/user.action';
import axios from 'restyped-axios';
import Auth from '../types/auth';
import {AUTH_SUCCESS} from '../actions/auth.action';
import {EarthbnbAPI} from '../../utils/api';

const api = axios.create<EarthbnbAPI>({
	baseURL: window.location.origin + '/api/'
});

const state: Auth = {
	token: localStorage.getItem('user-token') || '',
	status: ''
};

const getters = {
	signUpStatus: state => state.status
};

const actions = {
	[SIGN_UP_REQUEST]: ({commit, dispatch}, data) => {
		return new Promise((resolve, reject) => {
			commit(SIGN_UP_REQUEST);
			api.post('https://localhost:3000/api/signup', data)
				.then(res => {
					localStorage.setItem('user-token', res.data.auth_token);
					axios.defaults.headers.common.Authorization = 'Bearer ' + res.data.auth_token;
					commit(SIGN_UP_SUCCESS, res);
					commit(AUTH_SUCCESS, res);
					dispatch(USER_REQUEST);
					resolve(res);
				})
				.catch(err => {
					commit(SIGN_UP_ERROR, err);
					localStorage.removeItem('user-token');
					reject(err);
				});
		});
	}
};

const mutations = {
	[SIGN_UP_REQUEST]: state => {
		state.status = 'loading';
	},
	[SIGN_UP_SUCCESS]: (state, res) => {
		state.status = 'success';
		state.token = res.data.auth_token;
	},
	[SIGN_UP_ERROR]: state => {
		state.status = 'error';
	}
};

export default {
	state,
	getters,
	actions,
	mutations
};
