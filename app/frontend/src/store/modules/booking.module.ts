import {BOOKING_REQUEST, BOOKING_ERROR} from '../actions/booking.action';
import axios from 'restyped-axios';
import {EarthbnbAPI} from '../../utils/api';
import {TRIPS_REQUEST} from '../actions/trip.action';

const api = axios.create<EarthbnbAPI>({
	baseURL: window.location.origin + '/api/'
});
const state = {
	status: ''
};

const getters = {
	bookingStatus: state => state.status
};

const actions = {
	[BOOKING_REQUEST]: ({commit}, data) => {
		return new Promise((resolve, reject) => {
			commit(BOOKING_REQUEST);
			api.post('/trips', data)
				.then(res => {
					commit(TRIPS_REQUEST, res);
					resolve(res);
				})
				.catch(err => {
					commit(BOOKING_ERROR, err);
					reject(err);
				});
		});
	}
};

const mutations = {
	[BOOKING_REQUEST]: state => {
		state.status = 'loading';
	},
	[BOOKING_ERROR]: state => {
		state.status = 'error';
	}
};

export default {
	state,
	getters,
	actions,
	mutations
};
