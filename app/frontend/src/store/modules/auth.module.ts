import {AUTH_ERROR, AUTH_LOGOUT, AUTH_REQUEST, AUTH_SUCCESS} from '../actions/auth.action';
import {USER_REQUEST} from '../actions/user.action';
import axios from 'restyped-axios';
import Auth from '../types/auth';
import {EarthbnbAPI} from '../../utils/api';

const api = axios.create<EarthbnbAPI>({
	baseURL: window.location.origin + '/api/'
});

const state: Auth = {
	token: localStorage.getItem('user-token') || '',
	status: ''
};

const getters = {
	isAuthenticated: state => !!state.token,
	authStatus: state => state.status
};

const actions = {
	[AUTH_REQUEST]: ({commit, dispatch}, credentials) => {
		return new Promise((resolve, reject) => {
			commit(AUTH_REQUEST);
			api.post('/login', credentials)
				.then(res => {
					localStorage.setItem('user-token', res.data.auth_token);
					axios.defaults.headers.common.Authorization = 'Bearer ' + res.data.auth_token;
					commit(AUTH_SUCCESS, res);
					dispatch(USER_REQUEST);
					resolve(res);
				})
				.catch(err => {
					commit(AUTH_ERROR, err);
					localStorage.removeItem('user-token');
					reject(err);
				});
		});
	},
	[AUTH_LOGOUT]: ({commit}) => {
		return new Promise(resolve => {
			commit(AUTH_LOGOUT);
			localStorage.removeItem('user-token');
			resolve();
		});
	}
};

const mutations = {
	[AUTH_REQUEST]: state => {
		state.status = 'loading';
	},
	[AUTH_SUCCESS]: (state, res) => {
		state.status = 'success';
		state.token = res.data.auth_token;
	},
	[AUTH_ERROR]: state => {
		state.status = 'error';
	},
	[AUTH_LOGOUT]: state => {
		state.token = '';
	}
};

export default {
	state,
	getters,
	actions,
	mutations
};
