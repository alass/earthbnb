export interface User {
	status: string;
	profile: {
		[key: string]: Profile;
	};
}

export interface Profile {
	id: number;
	name: string;
	email: string;
	createdAt: string;
	updatedAt: string;
}
