export interface Room {
	id: number;
	title: string;
	location: string;
	price: number;
	images: {
		[key: string]: Image;
	};
	hostId: string;
	createdAt: string;
	updatedAt: string;
}

export interface Image {
	id: number;
	roomId: number;
	imgUrl: string;
	createdAt: string;
	updatedAt: string;
}
