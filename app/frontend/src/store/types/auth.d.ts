export default interface Auth {
	status: string;
	token: string;
}
