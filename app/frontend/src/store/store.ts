import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user.module';
import auth from './modules/auth.module';
import rooms from './modules/rooms.module';
import signup from './modules/signup.module';
import booking from './modules/booking.module';
import trips from './modules/trips.module';

Vue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		user,
		auth,
		signup,
		rooms,
		booking,
		trips
	}
});

export default store;
