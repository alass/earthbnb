export interface Base {
	[route: string]: any;
}

export interface Route {
	params: any;
	query: any;
	body: any;
	response: any;
}

export interface IndexedBase {
	[route: string]: {
		[method: string]: Route;
	};
}
