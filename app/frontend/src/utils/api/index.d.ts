export interface EarthbnbAPI {
	'/login': {
		POST: {
			params: {
				email: string;
				password: string;
			};
			response: {
				message?: string;
				auth_token?: string;
			};
		};
	};
	'/signup': {
		POST: {
			params: {
				email: string;
				name: string;
				password: string;
				password_confirmation: string;
			};
			response: {
				message?: string;
				auth_token?: string;
			};
		};
	};
	'/users/me': {
		GET: {
			params: {};
			response: object;
		};
	};
	'/rooms': {
		GET: {
			params: {};
			response: any[];
		};
	};
	'/trips': {
		POST: {
			params: {};
			response: any[];
		};
	};
	'/trips/:id': {
		DELETE: {
			params: {
				id: string;
			};
			response: any[];
		};
	};
}
