class TripSerializer < ActiveModel::Serializer
  # attributes to be serialized
  attributes :id, :check_in, :check_out, :created_at, :updated_at, :guest_id
  # model association
  has_one :room
end
