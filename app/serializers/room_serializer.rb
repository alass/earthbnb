class RoomSerializer < ActiveModel::Serializer
  # attributes to be serialized
  attributes :id, :title, :location, :price, :lat, :lng, :created_at, :updated_at, :host_id
  # model association
  has_many :images
end
