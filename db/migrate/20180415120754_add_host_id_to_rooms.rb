class AddHostIdToRooms < ActiveRecord::Migration[5.1]
  def change
    add_column :rooms, :host_id, :integer, index: true
    add_foreign_key :rooms, :users, column: :host_id
  end
end
