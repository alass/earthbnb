class ChangeRoomLatLngFieldsToNull < ActiveRecord::Migration[5.2]
  def change
  		change_column :rooms, :lat, :float, null: false, default: 0.0
    	change_column :rooms, :lng, :float, null: false, default: 0.0
  end
end
