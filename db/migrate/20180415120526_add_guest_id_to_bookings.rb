class AddGuestIdToTrips < ActiveRecord::Migration[5.1]
  def change
    add_column :trips, :guest_id, :integer, index: true
    add_foreign_key :trips, :users, column: :guest_id
  end
end
