# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require "uri"

User.destroy_all
Room.destroy_all
Trip.destroy_all
Image.destroy_all

u1 = User.create!(name: "Alassane", email: "bonjour@alassanecoly.fr", password_digest: User.digest("password"))
u2 = User.create!(name: "Jean-Baptiste", email: "jb@einrot.com", password_digest: User.digest("password"))
u3 = User.create!(name: "Matt", email: "matthew@top.com", password_digest: User.digest("password"))
u4 = User.create!(name: "Dan", email: "dan96@inator.com", password_digest: User.digest("password"))
u5 = User.create!(name: "Laura", email: "avelolo@uuid.com", password_digest: User.digest("password"))
u6 = User.create!(name: "Will", email: "willywonkaaa@factory.com", password_digest: User.digest("password"))
u7 = User.create!(name: "Manu", email: "manurose@gmail.com", password_digest: User.digest("password"))
u8 = User.create!(name: "Alex", email: "alexanderwang@wow.com", password_digest: User.digest("password"))
u9 = User.create!(name: "Katie", email: "kitkat@choco.com", password_digest: User.digest("password"))
u10 = User.create!(name: "Rachid", email: "rachidrocma@kyra.com", password_digest: User.digest("password"))
u11 = User.create!(name: "Brahim", email: "brh95@totali.com", password_digest: User.digest("password"))
u12 = User.create!(name: "Pakatou", email: "pakulupito@pasqual.com", password_digest: User.digest("password"))
u13 = User.create!(name: "Juliana", email: "juliana@rispa.com", password_digest: User.digest("password"))
u14 = User.create!(name: "Nicolas", email: "nico22@fulo.com", password_digest: User.digest("password"))
u15 = User.create!(name: "Issa", email: "issaaa@lima.com", password_digest: User.digest("password"))
u16 = User.create!(name: "Duy-Thien", email: "dytens@truong.com", password_digest: User.digest("password"))

r1 = Room.create!(
  title:    "Cute Private Guesthouse in Residential East Village",
  location: "Manhattan, NY",
  price:    165.00,
  lat:			40.779897,
  lng:			-73.968565,
  host_id:  u1.id
)

r2 = Room.create!(
  title:    "Large, Beautiful Apartment in Prime Location in Brooklyn",
  location: "Brooklyn, NY",
  price:    90.00,
  lat:			40.6781784,
  lng:			-73.9441579,
  host_id:  u3.id,
)

r3 = Room.create!(title:    "Times Square Apartment with a View",
                  location: "Manhattan, NY",
                  price:    57.00,
                  lat: 			40.7830603,
                  lng:			-73.9712488,
                  host_id:  u4.id,)

r4 = Room.create!(
  title:    "Loft Near Downtown SF",
  location: "San Francisco, CA",
  price:    120.00,
  lat: 			37.774929,
  lng:			-122.419416,
  host_id:  u8.id
)

r5 = Room.create!(title:    "Stunning apartment in the Heart of the South Side of Dublin City",
                  location: "Dublin, Ireland",
                  price:    115.00,
                  lat:			53.349805,
                  lng:			-6.260310,
                  host_id:  u2.id)

r6 = Room.create!(title:    "Room in Trendy Apartment available next to Trafalgar Square",
                  location: "London, UK",
                  price:    75.00,
                  lat:			51.5073509,
                  lng:			-0.1277583,
                  host_id:  u1.id)

r7 = Room.create!(
  title:    "Cozy and Bright Apartment near Union Square",
  location: "San Francisco, CA",
  price:    215.00,
  lat:			37.787994,
  lng:			-122.407437,
  host_id:  u9.id
)

r8 = Room.create!(
  title:    "Apartment on Champs Elysées avenue",
  location: "Paris, France",
  price:    220.00,
  lat:			48.866667,
  lng:			2.333333,
  host_id:  u8.id
)

r9 = Room.create!(title:    "Great Home with Private Pool Available to Share",
                  location: "Dhahran, Saudi Arabia",
                  lat:			26.288769,
                  lng:			50.114101,
                  price:    100.00,
                  host_id:  u8.id)

r10 = Room.create!(
  title:    "Inner City bedroom Apartment",
  location: "Melbourne, Australia",
  lat:			-37.81361100000001,
  lng:			144.96305600000005,
  price:    80.00,
  host_id:  u5.id
)

r11 = Room.create!(
  title:    "Beautiful Apartment in Gangnam-Gu",
  location: "Seoul, South Korea",
  lat:			37.566535,
  lng:			126.9779692,
  price:    110.00,
  host_id:  u7.id
)

r12 = Room.create!(
   title:    "4 minutes to Shinjuku: New Tokyo Apartment 403",
   location: "Tokyo, Japan",
   lat:			35.709026,
   lng:			139.731992,
   price:    90.00,
   host_id:  u16.id
 )

r13 = Room.create!(
  title:    "Countryfield house with vineyards",
  location: "Sicily, Italy",
  lat:			37.599994,
  lng:			14.015356,
  price:    35.00,
  host_id:  u13.id
)
r14 = Room.create!(
    title:    "Colorful Mid-Century Inspired Apartment",
    location: "Barcelona, Spain",
    lat:			41.390205,
    lng:			2.154007,
    price:    100.00,
    host_id:  u13.id
)

r15 = Room.create!(
    title:    "Studio with Breathtaking View on Eiffel Tower",
    location: "Paris, France",
    lat:			48.858093,
    lng:			2.294694,
    price:    250.00,
    host_id:  u1.id
)

r16 = Room.create!(
    title:    "【超好立地おしゃれタウン】渋谷3分、代官山5分、恵比寿8分。女の子らしい、インスタ映えする部屋",
    location: "Tokyo, Japan",
    lat:			35.689487,
    lng:			139.691706,
    price:    180.00,
    host_id:  u16.id
)

r17 = Room.create!(
    title:    "Master Bedroom + Private Bath — Paris House",
    location: "Paris, France",
    lat:			48.870502,
    lng:			2.304897,
    price:    330.00,
    host_id:  u1.id
)

r18 = Room.create!(
    title:    "隐庐 故宫旁 日式花园庭院 新中式LOFT套房",
    location: "Beijing, China",
    lat:			39.9042004,
    lng:			116.407396,
    price:    40.00,
    host_id:  u14.id
)
r19 = Room.create!(
    title:    "Huge City Center Penthouse with rooftop Jaccuzi",
    location: "Johannesburg, South Africa",
    lat:			-26.2041028,
    lng:			28.047305100000017,
    price:    200.00,
    host_id:  u11.id
)
r20 = Room.create!(
    title:    "Spacious apartment in South Mumbai",
    location: "Bombay, India",
    lat:			19.0759844,
    lng:			72.877656,
    price:    70.00,
    host_id:  u12.id
)

date1 = Date.new(2018, 4, 5)
date2 = Date.new(2018, 5, 7)
date3 = Date.new(2019, 6, 20)
date4 = Date.new(2019, 7, 13)
date5 = Date.new(2018, 8, 3)
date6 = Date.new(2018, 9, 17)

date7 = Date.new(2018, 4, 22)
date8 = Date.new(2018, 5, 30)
date9 = Date.new(2019, 6, 27)
date10 = Date.new(2019, 7, 28)
date11 = Date.new(2018, 8, 15)
date12 = Date.new(2018, 9, 26)

Trip.create!(room_id: r1.id, check_in: date1, check_out: date7, guest_id: u1.id)
Trip.create!(room_id: r2.id, check_in: date1, check_out: date2, guest_id: u6.id)
Trip.create!(room_id: r3.id, check_in: date5, check_out: date6, guest_id: u6.id)
Trip.create!(room_id: r4.id, check_in: date8, check_out: date10, guest_id: u2.id)
Trip.create!(room_id: r5.id, check_in: date10, check_out: date11, guest_id: u6.id)
Trip.create!(room_id: r6.id, check_in: date5, check_out: date4, guest_id: u10.id)
Trip.create!(room_id: r7.id, check_in: date3, check_out: date4, guest_id: u10.id)
Trip.create!(room_id: r8.id, check_in: date1, check_out: date2, guest_id: u5.id)
Trip.create!(room_id: r9.id, check_in: date9, check_out: date4, guest_id: u7.id)
Trip.create!(room_id: r10.id, check_in: date5, check_out: date11, guest_id: u4.id)
Trip.create!(room_id: r11.id, check_in: date8, check_out: date12, guest_id: u3.id)
Trip.create!(room_id: r12.id, check_in: date1, check_out: date2, guest_id: u11.id)
Trip.create!(room_id: r13.id, check_in: date5, check_out: date6, guest_id: u15.id)
Trip.create!(room_id: r14.id, check_in: date8, check_out: date10, guest_id: u12.id)
Trip.create!(room_id: r15.id, check_in: date10, check_out: date11, guest_id: u13.id)
Trip.create!(room_id: r16.id, check_in: date5, check_out: date4, guest_id: u14.id)
Trip.create!(room_id: r17.id, check_in: date3, check_out: date4, guest_id: u15.id)
Trip.create!(room_id: r18.id, check_in: date1, check_out: date2, guest_id: u1.id)
Trip.create!(room_id: r19.id, check_in: date9, check_out: date4, guest_id: u2.id)
Trip.create!(room_id: r20.id, check_in: date5, check_out: date11, guest_id: u7.id)

Image.create!(
    room_id: r1.id,
    img_url: "https://fortunedotcom.files.wordpress.com/2014/12/27496569_original.jpg"
)
Image.create!(
    room_id: r1.id,
    img_url: "https://i.pinimg.com/originals/bf/a7/dd/bfa7dd89e5d4ed610aa0efa1cd735cef.jpg"
)
Image.create!(
    room_id: r1.id,
    img_url: "http://viafrica.co.ke/wp-content/uploads/2017/07/Pic-1.jpg"
)
Image.create!(
    room_id: r2.id,
    img_url: "https://media.architecturaldigest.com/photos/57c750354b3d33ab5ee2806b/master/pass/zendaya-airbnb.jpg"
)
Image.create!(
    room_id: r3.id,
    img_url: "https://cdn.vox-cdn.com/thumbor/ZLXqcthQatDfqAWueffQkSx-axU=/0x0:1000x646/1200x800/filters:focal(420x243:580x403)/cdn.vox-cdn.com/uploads/chorus_image/image/51562955/Screen_20Shot_202016-10-27_20at_205.28.16_20PM.0.png"
)
Image.create!(
    room_id: r3.id,
    img_url: "http://photos.clevescene.com/wp-content/uploads/2015/06/214.jpg"
)
Image.create!(
    room_id: r4.id,
    img_url: "https://www.notreloft.com/images/2014/09/loft-airbnb-paris-00600.jpg"
)
Image.create!(
    room_id: r5.id,
    img_url: "https://nit.pt/wp-content/uploads/2017/10/cc06edd09eb78978c2380a479d500439.jpg"
)
Image.create!(
    room_id: r6.id,
    img_url: "http://resize2-elle.ladmedia.fr/img/var/plain_site/storage/images/deco/reportages/city-guide/airbnb-biarritz-les-appartements-lofts-et-maisons-de-reve-a-biarritz/83299433-1-fre-FR/Airbnb-Biarritz-20-appartements-lofts-et-maisons-de-reve-a-Biarritz.jpg"
)
Image.create!(
    room_id: r7.id,
    img_url: "https://patrimoine.lesechos.fr/medias/2017/04/14/2079703_lappartement-de-la-semaine-un-loft-de-300-m2-a-bordeaux-web-tete-0211960300779.jpg"
)
Image.create!(
    room_id: r8.id,
    img_url: "https://www.lipstiq.com/wp-content/uploads/2017/11/airbnb.jpg"
)
Image.create!(
    room_id: r9.id,
    img_url: "https://images.fastcompany.net/image/upload/w_1280,f_auto,q_auto,fl_lossy/fc/3063689-poster-p-1-audi-and-airbnb-are-staging-the-ultimate-death-valley-test-drive-and-they-want-you-behind-th.jpg"
)
Image.create!(
    room_id: r10.id,
    img_url: "https://www.vacationkey.com/photos/5/4/54449-3.jpg"
)
Image.create!(
    room_id: r11.id,
    img_url: "http://www.lonelyplanet.com/news/wp-content/uploads/2017/07/Strasbourg-Airbnb.jpg"
)
Image.create!(
    room_id: r12.id,
    img_url: "https://media.timeout.com/images/103116693/image.jpg"
)
Image.create!(
    room_id: r12.id,
    img_url: "https://media.timeout.com/images/103116694/image.jpg"
)
Image.create!(
    room_id: r13.id,
    img_url: "https://feedmycomplex.files.wordpress.com/2014/10/67fed66a_original.jpg"
)
Image.create!(
    room_id: r14.id,
    img_url: "https://imagesvc.timeincapp.com/v3/mm/image?url=https%3A%2F%2Ffortunedotcom.files.wordpress.com%2F2017%2F10%2Ftec11_a1.jpg&w=2200&q=70"
)
Image.create!(
    room_id: r15.id,
    img_url: "http://www.eurasiatimes.org/wp-content/uploads/2017/12/Airbnb-Paris.jpg"
)
Image.create!(
    room_id: r16.id,
    img_url: "https://media.timeout.com/images/103116698/image.jpg"
)
Image.create!(
    room_id: r16.id,
    img_url: "https://media.timeout.com/images/103116700/image.jpg"
)
Image.create!(
    room_id: r17.id,
    img_url: "https://3.bp.blogspot.com/-euqY_ffES04/WYN2B6WbMqI/AAAAAAAAAEU/sOwXvWzJmgcQawYEUoV1HnqJRIrA9W0IwCLcBGAs/s1600/best%2Bairbnb%2Bin%2Bparis%252C%2Bthe%2Barty%2Bone%2BMAIN%252C%2Bwww.calmctravels.com.jpg"
)
Image.create!(
    room_id: r18.id,
    img_url: "http://www.thisrenegadelove.com/wp-content/uploads/2015/07/AirBnB-Paris1.jpg"
)
Image.create!(
    room_id: r19.id,
    img_url: "https://media.timeout.com/images/103585764/1372/772/image.jpg"
)
Image.create!(
    room_id: r20.id,
    img_url: "https://theshootingstar.files.wordpress.com/2014/04/dsc03984.jpg"
)