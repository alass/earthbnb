# Earthbnb 

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) [![CircleCI](https://circleci.com/gh/alassanecoly/earthbnb.svg?style=shield)](https://circleci.com/gh/alassanecoly/earthbnb) [![Maintainability](https://api.codeclimate.com/v1/badges/5c332fa8b0639531cb4f/maintainability)](https://codeclimate.com/github/alassanecoly/earthbnb/maintainability) [![Coverage Status](https://coveralls.io/repos/github/alassanecoly/earthbnb/badge.svg?branch=develop)](https://coveralls.io/github/alassanecoly/earthbnb?branch=develop) [![Dependency Status](https://gemnasium.com/badges/github.com/alassanecoly/earthbnb.svg)](https://gemnasium.com/github.com/alassanecoly/earthbnb)

Earthbnb is a full-stack web application inspired by Airbnb, but aimed at aliens instead of humans.

## Getting Started

### Prerequisites

#### Docker

Docker installation is required, see the official [installation docs](https://docs.docker.com/install/).

First step is to build the environment and install rails (or anything else…) before rebuilding the complete environment :

```Sh
docker-compose build
```

Afterwards building is only necessary when installing new gem dependencies.

Next we need to setup the database. First enter into the container using :

```Sh
docker-compose run web bash
```
Then setup the database with :

```Sh
rails db:setup
```

######  Linux specific

From host, to 'get ownership back' for the generated files (with a `rails generate` for instance), this command finds all files owned by root in the current directory and chown them back to `given_user`:
(Postgres directory will stay own by root, this is normal !)

```bash
find . -user root | xargs sudo chown given_user:given_user
```

#### Environment variables

This app uses environment variables for Algolia API keys and database host. Environment variables are managed with Figaro gem. Install Figaro using :

```bash
figaro install
```

This creates a commented `config/application.yml` file and adds it to your `.gitignore`. Add your own configuration to this file like this :

```yaml
ALGOLIA_APP_ID: "RENxxxxx"
ALGOLIA_API_KEY: "76b1xxxxxxxxxxx"
DATABASE_HOST: "db"
```

Everything is set up. You can now start using the application using :

```sh
docker-compose up
```

You can access to the app at : https://localhost:3000

## Authors

- Alassane Coly-Arogundade
- Mathieu Ba
- Emmanuel Rose

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
