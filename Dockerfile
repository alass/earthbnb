# Use this file with Docker
# https://docs.docker.com/engine/installation/
FROM ruby:2.5.0
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -qq && apt-get install -y nodejs && apt-get install yarn
RUN apt-get install -y build-essential libpq-dev 
RUN apt-get install -y cmake

# Force Bundler to work in parallel
RUN bundle config --global jobs 4
RUN echo 'gem: --no-document' > /root/.gemrc

# Generate SSL cert
RUN openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 365 -keyout localhost.key -out localhost.crt \
  -subj "/C=UK/ST=Warwickshire/L=Leamington/O=OrgName/OU=IT Department/CN=example.com"

RUN mkdir /earthbnb
WORKDIR /earthbnb
COPY Gemfile /earthbnb/Gemfile
COPY Gemfile.lock /earthbnb/Gemfile.lock

# Install Ruby, Yarn & NPM dependencies
RUN bundle install
COPY . /earthbnb
RUN yarn install