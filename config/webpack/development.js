process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const webpack = require('webpack');
const environment = require('./environment');

environment.plugins.append('Provide', new webpack.ProvidePlugin({
	$: 'jquery',
	jQuery: 'jquery',
	jquery: 'jquery',
	'window.Tether': 'tether',
	Popper: ['popper.js', 'default'],
	ActionCable: 'actioncable',
	Vue: 'vue',
	VueResource: 'vue-resource'
}));

const config = environment.toWebpackConfig();

config.resolve.alias = {
	jquery: 'jquery/src/jquery',
	vue: 'vue/dist/vue.js',
	vueResource: 'vue-resource/dist/vue-resource'
};

// Export the updated config
module.exports = config;
