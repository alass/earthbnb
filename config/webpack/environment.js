const {environment} = require('@rails/webpacker');
const typescript = require('./loaders/typescript');
const vue = require('./loaders/vue');
const lodash = require('./loaders/lodash');

environment.loaders.append('vue', vue);
environment.loaders.append('typescript', typescript);
environment.loaders.append('lodash', lodash);
module.exports = environment;
