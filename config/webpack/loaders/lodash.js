module.exports = {
	test: /\.js$/,
	use: [{
		loader: 'babel-loader',
		options: {
			plugins: ['lodash', 'lodash-webpack-plugin'],
			presets: [['env', {modules: false, targets: {node: 4}}]]
		}
	}],
	exclude: /node_modules/
};
