module.exports = {
	test: /\.(sass|css)$/i,
	use: [
		{loader: 'css-loader', options: {minimize: process.env.NODE_ENV === 'production'}},
		'sass-loader',
		'vue-style-loader',
		'style-loader'
	]
};
