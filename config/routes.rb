Rails.application.routes.draw do
  root to: 'application#index', format: false
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Namespace the controllers without affecting the URI
  namespace :api do
    scope module: :v1, constraints: ApiVersion.new('v1', true) do
      get "users/me", to: "users#get_me"
      put "users/me", to: "users#update_me"
      resources :rooms
      resources :trips
      resources :users
      resources :images
      post "login", to: "authentication#authenticate"
      post "signup", to: "users#create"
    end
  end
  # Catch all for HTML 5 history routing. This must be the last route.
  get '/*path', to: 'application#index', format: false
end
