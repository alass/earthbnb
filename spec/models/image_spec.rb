require 'rails_helper'

RSpec.describe Image, type: :model do
  # ensure an image record belongs to a single room
  it { should belong_to(:room) }
  # ensure columns are present before saving
  it { should validate_presence_of(:img_url) }
end
