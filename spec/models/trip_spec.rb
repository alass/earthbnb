require "rails_helper"

# Test suite for the Item model
RSpec.describe Trip, type: :model do
  # Association test
  # ensure an item record belongs to a single Room & User record
  it { should belong_to(:room) }
  # Validation test
  # ensure columns are present before saving
  it { should validate_presence_of(:check_in) }
  it { should validate_presence_of(:check_in) }
end
