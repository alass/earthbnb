require "rails_helper"

RSpec.describe "Users API", type: :request do
  let(:user) { build(:user) }
  let!(:users) { create_list(:user, 10) }
  let(:user_id) { users.first.id }
  let(:header) { {"Authorization" => token_generator(users.first.id),
                  "HTTPS" => "on"} }
  let(:headers) { valid_headers.except("Authorization") }
  let(:valid_attributes) do
    attributes_for(:user, password_confirmation: user.password)
  end

  # User signup test suite
  describe "POST /api/signup" do
    context "when valid request" do
      before { post "/api/signup", params: valid_attributes.to_json, headers: headers }

      it "creates a new user" do
        expect(response).to have_http_status(201)
      end

      it "returns success message" do
        expect(json["message"]).to match(/Account created successfully/)
      end

      it "returns an authentication token" do
        expect(json["auth_token"]).not_to be_nil
      end
    end

    context "when invalid request" do
      before { post "/api/signup", params: {}, headers: headers }

      it "does not create a new user" do
        expect(response).to have_http_status(422)
      end

      it "returns failure message" do
        expect(json["message"])
            .to match(/Validation failed: Password can't be blank, Name can't be blank, Email can't be blank, Password digest can't be blank/)
      end
    end
  end
  describe "GET /api/users/:id" do
    before { get "/api/users/#{user_id}", params: {}, headers: headers }

    context "when the record exists" do
      it "returns the user" do
        expect(json).not_to be_empty
        expect(json["id"]).to eq(user_id)
      end

      it "returns status code 200" do
        expect(response.status).to be(200)
      end
    end

    context "when the record does not exist" do
      let(:user_id) { 100 }

      it "returns status code 404" do
        expect(response.status).to be(404)
      end
    end
  end
  # Test suite for PUT /api/users
  describe "PUT /api/users/me" do
    let(:valid_attributes) { {name: "Hello"}.to_json }

    context "when the record exists" do
      before { put "/api/users/me", params: valid_attributes, headers: header }

      it "updates the record if logged in" do
        expect(response.body).to be_empty
      end

      it "returns status code 204 if logged in" do
        expect(response.status).to be(204)
      end
    end

    context "when the request is invalid" do
      let(:invalid_attributes) { {title: nil}.to_json }
      before { put "/api/users/me", params: invalid_attributes, headers: headers }

      it "returns status code 422" do
        expect(response.status).to be(422)
      end
    end
  end
  # Test suite for GET /api/users/me
  describe "GET /api/users/me" do
    before { get "/api/users/me", params: {}, headers: header }

    context "when the record exists" do
      it "returns the user" do
        expect(json).not_to be_empty
        expect(json["id"]).to eq(user_id)
      end

      it "returns status code 200" do
        expect(response.status).to be(200)
      end
    end
  end
end
