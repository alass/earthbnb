require "rails_helper"

RSpec.describe "Images API", type: :request do
  # add image
  let(:image) { create(:image) }
  let(:user) { create(:user) }
  let!(:users) { create_list(:user, 10) }
  let!(:room) { create(:room, host_id: user.id) }
  # initialize test data
  let!(:images) { create_list(:image, 10, room_id: room.id) }
  let(:image_id) { images.first.id }
  # authorize request
  let(:headers) { valid_headers }

  # Test suite for GET /api/images
  describe "GET /api/images" do
    # make HTTP get request before each example
    before { get "/api/images", params: {}, headers: headers }

    it "returns images" do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it "returns status code 200" do
      expect(response.status).to be(200)
    end
  end

  # Test suite for GET /api/images/:id
  describe "GET /api/images/:id" do
    before { get "/api/images/#{image_id},", params: {}, headers: headers }

    context "when the record exists" do
      it "returns the images" do
        expect(json).not_to be_empty
        expect(json["id"]).to eq(image_id)
      end

      it "returns status code 200" do
        expect(response.status).to be(200)
      end
    end

    context "when the record does not exist" do
      let(:image_id) { 100 }

      it "returns status code 404" do
        expect(response.status).to be(404)
      end

      it "returns a not found message" do
        expect(response.body).to match(/Couldn't find Image/)
      end
    end
  end

  # Test suite for POST /api/images
  describe "POST /api/images" do
    # valid payload
    let(:valid_attributes) do
      {img_url: "https://photos.com/img.png", room_id: room.id}.to_json
    end

    context "when the request is valid" do
      before { post "/api/images", params: valid_attributes, headers: headers }

      it "creates a image" do
        expect(json["img_url"]).to eq("https://photos.com/img.png")
      end

      it "returns status code 201" do
        expect(response.status).to be(201)
      end
    end

    context "when the request is invalid" do
      let(:invalid_attributes) { {img_url: nil}.to_json }
      before { post "/api/images", params: invalid_attributes, headers: headers }

      it "returns status code 404" do
        expect(response.status).to be(404)
      end

      it "returns a validation failure message" do
        expect(json["message"])
            .to match(/Couldn't find Room without an ID/)
      end
    end
  end

  # Test suite for PUT /api/images/:id
  describe "PUT /api/images/:id" do
    let(:valid_attributes) { {title: "React"}.to_json }

    context "when the record exists" do
      before { put "/api/images/#{image_id}", params: valid_attributes, headers: headers }

      it "updates the record" do
        expect(response.body).to be_empty
      end

      it "returns status code 204" do
        expect(response.status).to be(204)
      end
    end
  end

  # Test suite for DELETE /api/images/:id
  describe "DELETE /api/images/:id" do
    before { delete "/api/images/#{image_id}", params: {}, headers: headers }

    it "returns status code 204" do
      expect(response.status).to be(204)
    end
  end
end
