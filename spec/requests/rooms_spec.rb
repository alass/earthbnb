require "rails_helper"

RSpec.describe "Rooms API", type: :request do
  # add rooms owner
  let(:user) { create(:user) }
  # initialize test data
  let!(:rooms) { create_list(:room, 10, host_id: user.id) }
  let(:room_id) { rooms.first.id }
  # authorize request
  let(:headers) { valid_headers }

  # Test suite for GET /api/rooms
  describe "GET /api/rooms" do
    # make HTTP get request before each example
    before { get "/api/rooms", params: {}, headers: headers }

    it "returns rooms" do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it "returns status code 200" do
      expect(response.status).to be(200)
    end
  end

  # Test suite for GET /api/rooms/:id
  describe "GET /api/rooms/:id" do
    before { get "/api/rooms/#{room_id},", params: {}, headers: headers }

    context "when the record exists" do
      it "returns the rooms" do
        expect(json).not_to be_empty
        expect(json["id"]).to eq(room_id)
      end

      it "returns status code 200" do
        expect(response.status).to be(200)
      end
    end

    context "when the record does not exist" do
      let(:room_id) { 100 }

      it "returns status code 404" do
        expect(response.status).to be(404)
      end

      it "returns a not found message" do
        expect(response.body).to match(/Couldn't find Room/)
      end
    end
  end

  # Test suite for POST /api/rooms
  describe "POST /api/rooms" do
    # valid payload
    let(:valid_attributes) do
      {title: "Learn TypeScript", location: "Iceland", price: "20", host_id: user.id.to_s}.to_json
    end

    context "when the request is valid" do
      before { post "/api/rooms", params: valid_attributes, headers: headers }

      it "creates a room" do
        expect(json["title"]).to eq("Learn TypeScript")
      end

      it "returns status code 201" do
        expect(response.status).to be(201)
      end
    end

    context "when the request is invalid" do
      let(:invalid_attributes) { {title: nil}.to_json }
      before { post "/api/rooms", params: invalid_attributes, headers: headers }

      it "returns status code 422" do
        expect(response.status).to be(422)
      end

      it "returns a validation failure message" do
        expect(json["message"])
          .to match(/Validation failed: Title can't be blank/)
      end
    end
  end

  # Test suite for PUT /api/rooms/:id
  describe "PUT /api/rooms/:id" do
    let(:valid_attributes) { {title: "React"}.to_json }

    context "when the record exists" do
      before { put "/api/rooms/#{room_id}", params: valid_attributes, headers: headers }

      it "updates the record" do
        expect(response.body).to be_empty
      end

      it "returns status code 204" do
        expect(response.status).to be(204)
      end
    end
  end

  # Test suite for DELETE /api/rooms/:id
  describe "DELETE /api/rooms/:id" do
    before { delete "/api/rooms/#{room_id}", params: {}, headers: headers }

    it "returns status code 204" do
      expect(response.status).to be(204)
    end
  end
end
