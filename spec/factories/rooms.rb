require "factory_bot"

FactoryBot.define do
  factory :room do
    user
    title { Faker::Lorem.word }
    location { Faker::Address.city }
    price { Faker::Number.decimal(2, 3) }
    created_at { Faker::Date.between(10.days.ago, Date.today) }
    updated_at { Faker::Date.between(2.days.ago, Date.today) }
  end
end
