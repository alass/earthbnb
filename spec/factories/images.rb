require "factory_bot"

FactoryBot.define do
  factory :image do
    room
    img_url { Faker::Internet.url }
    created_at { Faker::Date.between(10.days.ago, Date.today) }
    updated_at { Faker::Date.between(2.days.ago, Date.today) }
  end
end
