require "factory_bot"

FactoryBot.define do
  factory :trip do
    room
    check_in { Faker::Date.between(10.days.ago, Date.today) }
    check_out { Faker::Date.between(Date.today, 3.months.from_now) }
    created_at { Faker::Date.between(10.days.ago, Date.today) }
    updated_at { Faker::Date.between(2.days.ago, Date.today) }
  end
end
