# Earthbnb Features List (WIP)

Airbnb Clone, being developed, replicating the core functionality's of Airbnb Portal, is an online marketplace, allowing its users to search for A-class properties across the globe and avail the luxuries of a niche lifestyle.

 ## V0.1

| END-USER FEATURES | DESCRIPTION                                              |
| ----------------- | -------------------------------------------------------- |
| Sign Up           | User can sign up through <br> • Email                    |
| Sign In           | User can sign in through <br> • Email                    |
| List Your Space   | Search by <br> • Destination                             |
| Host              | Host has following functionalities : <br> • Manage rooms |
| Account           | • Booking history                                        |
| Profile           | • Edit profile                                           |



## V0.2

| END-USER FEATURES     | DESCRIPTION                                                  |
| --------------------- | ------------------------------------------------------------ |
| List Your Space       | Search by <br>  • Check In and Check Out date                |
| Details of Travelling | • Current trip                                               |
| Host                  | Host has following functionalities : <br> • Manage rooms <br>     • Amenities / sleeping arrangements / house rules |
| Reset password        | User can reset his password                                  |
| Wishlist              | User can manage his wishlist                                 |


## V0.3

| END-USER FEATURES | DESCRIPTION                                                  |
| ----------------- | ------------------------------------------------------------ |
| Sign Up           | User can sign up through <br> • Facebook <br> • Google       |
| Sign In           | User can sign in through <br> • Facebook <br> • Google       |
| List Your Space   | Search by <br> • No. of Guests <br> • Price <br> • Property type |
| Reviews           | User can review a room / host                                |

## V0.4

| END-USER FEATURES | DESCRIPTION                                       |
| ----------------- | ------------------------------------------------- |
| Message System    | Inbox for each user                               |
| Notifications     | Host receive notifications when guest book a room |
| Language Setting  | User can change language preference               |
