# API Enpoints

Base URL : https://{baseurl}/api

## v0.1

### Auth / Users

| Endpoint                | Functionality                    | Parameters                                             |
| ----------------------- | -------------------------------- | ------------------------------------------------------ |
| `POST` /signup{params}  | Sign up (Create User)            | {*:name, *:email, *:password, *:password_confirmation} |
| `POST` /login{params}   | Login                            | {*:email, *:password}                                  |
| `GET` /users/:id        | Get user informations            | N/A                                                    |
| `GET`/users/me          | Get current user informations    | N/A                                                    |
| `PUT` /users/me{params} | Update current user informations | {:name, :email}                                        |

### Rooms

| Endpoint                 | Functionality | Parameters                     |
| ------------------------ | ------------- | ------------------------------ |
| `GET` /rooms/:id         | Get room      | N/A                            |
| `PUT` /rooms/:id{params} | Update room   | {:title, :location, :price}    |
| `DELETE`/rooms/:id       | Delete room   | N/A                            |
| `POST` /rooms{params}    | Create room   | {*:title, *:location, *:price} |
| `GET` /rooms             | Get all rooms | N/A                            |

### Trips

| Endpoint                 | Functionality            | Parameters |
| ------------------------ | ------------------------ | ------------------------ |
| `GET` /trips/:id      | Get trip              | N/A |
| `DELETE`/trips/:id    | Delete trip           | N/A |
| `GET` /trips | Get all current user trips | N/A |
| `PUT`/trips/:id{params} | Update trip | {:room_id, :check_in, :check_out} |
| `POST`/trips{params} | Create trip | {*:room_id, *:check_in, *:check_out} |

### Images

| Endpoint              | Functionality     | Parameters             |
| --------------------- | ----------------- | ---------------------- |
| `POST`/images{params} | Post room image   | {*:img_url, *:room_id} |
| `PUT`/images{params}  | Update room image | {:img_url, :room_id}   |
| `DELETE` /images/:id  | Delete room image | N/A                    |

(*) required parameter